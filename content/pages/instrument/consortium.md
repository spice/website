Title: The SPICE consortium
Summary: Presentation of the SPICE consortium.
Slug: instrument/consortium

The SPICE consortium includes:





<table class="table table-striped table-hover ">
  <thead>
    <tr>
      <th>Institute</th>
      <th>Place</th>
      <th>Country</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>Institut d’Astrophysique Spatiale</td>
      <td>Orsay</td>
      <td>France</td>
    </tr>
    <tr>
      <td>Rutherford Appleton Laboratory</td>
      <td>Didcot</td>
      <td>UK</td>
    </tr>
    <tr>
      <td>Institute of Theoretical Astrophysics, University of Oslo</td>
      <td>Oslo</td>
      <td>Norway</td>
    </tr>
    <tr>
      <td>Max-Planck Institut für Sonnensystemforschung</td>
      <td>Göttingen</td>
      <td>Germany</td>
    </tr>
    <tr>
      <td>NASA Goddard Space Flight Center</td>
      <td>Washington DC</td>
      <td>USA</td>
    </tr>
    <tr>
      <td>Southwest Research Institute</td>
      <td>Boulder</td>
      <td>USA</td>
    </tr>
    <tr>
      <td>Physikalisch Meteorologisches Observatorium</td>
      <td>Davos</td>
      <td>Switzerland</td>
    </tr>
    <tr>
      <td>Fraunhofer Institut für Angewandte Optik und Feinmechanik</td>
      <td>Jena</td>
      <td>Germany</td>
    </tr>
    <tr>
      <td>Physikalisch-Technische Bundesanstalt</td>
      <td>Berlin</td>
      <td>Germany</td>
    </tr>
  </tbody>
</table> 
