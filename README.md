# Source for the SPICE website (draft static version)

This is the git repository for a draft static version of the Solar Orbiter/SPICE website.
It contains the source files needed to build and deploy this website at the different SPICE consortium members institutes.
**This is not currently used for the [SPICE](https://spice-web.ias.u-psud.fr/) website**.



## Dependencies

* [Pelican](http://www.getpelican.com)
* Python, with markdown module


## How to get and modify the source code

* Clone the Git repository (only the first time).
* Modify files in the `content` directory as needed.
* Submit yout changes back to the IAS repository with `git push`.
* Get changes from remote sites with `git pull`.

## How to deploy the site

* Use `make devserver` to launch a local development server
* Use `make rsync_upload` (with appropriate configuration, see the `SITE` variable in the `Makefile`) to upload to your production server, for example `make SITE=ias rsync_upload` to upload to the IAS server.
