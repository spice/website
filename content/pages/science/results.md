Title: SPICE scientific results
Summary: Presentation of the scientific results from SPICE observations.
Slug: science/results

You will be able to find here results from SPICE observations.
These can be links to published papers, presentations made in workshops/conferences etc.
Maybe propose some science nuggets?
