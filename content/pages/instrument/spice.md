Title: The SPICE instrument
Summary: Presentation of the SPICE instrument.
Slug: instrument/spice

The Spectral Imaging of the Coronal Environment (SPICE) instrument is an EUV imaging spectrograph designed to observe both the solar disk and the corona to remotely characterize plasma properties at and near the Sun. Specific scientific topics to be addressed by SPICE include studies of solar wind origin by matching in-situ composition signatures in solar wind streams to surface feature composition, studies of the physical processes that inject material from closed structures into solar wind streams, and studies of SEP source regions by imaging remotely the suprathermal ions thought to be seed populations of SEPs.


