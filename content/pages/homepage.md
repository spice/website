Title: Welcome to the SPICE website!
Slug: homepage
Summary: Homepage for website
Save_as: index.html

![SPICE logo](images/logos/logo-spice.png)

This is an example for the SPICE website.

It has been created using [Pelican](http://getpelican.com/), a static site generator.

This is only to give a framework of how the website could look like, providing YOUR inputs.

You will be able to find here:

* information about the instrument (what it does, who is behind SPICE etc)
* information about the science (what are the science objectives of SPICE, the results)
* information about the data (where to find the data, documentation)
* other information such as: meetings, help, outreach activities, etc.

<a class="btn btn-warning">Warning</a> This site is currently for testing purposes only.
