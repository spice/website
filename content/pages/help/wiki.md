Title: Wiki
Summary: Wiki.
Slug: help/wiki

The SPICE instrument wiki is available following [this link](https://spice.ias.u-psud.fr).
It is used for storing documents and other working information about the project.

Please note that some sections of the wiki are available only to the SPICE team, with a generic or with individual logins.
