#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

AUTHOR = u'SPICE consortium'
SITENAME = u'SPICE'

# leave this for dev server; variable overwritten if publishing to prod server
SITEURL = u'http://localhost:8000/'

PATH = 'content'

TIMEZONE = 'Europe/Paris'

DEFAULT_LANG = u'en'

DEFAULT_DATE = u'fs'  # use file date as article date

# Subdirectories of content/ to be copied to output
STATIC_PATHS = ['images', 'documents']

#Menu Items - configured by Miho
DISPLAY_PAGES_ON_MENU = False
DISPLAY_CATEGORIES_ON_MENU = False
# for one-level menu
#MENUITEMS = (
    #('Home', '/'),
    #('Instrument', '/pages/instrument/spice.html'),
    #('Science','/pages/science/objectives.html'),
    #('Meetings','/pages/meetings.html'),
    #('Operations', '/pages/operations/instrument-status.html'),
    #('Data Archives','/pages/data/archives.html'),
    #('Data Analysis','/pages/data/analysis-software.html'),
    #('Outreach','/pages/outreach.html'),
    #('Help','/pages/help/helpdesk.html')
#)
# for nested menus (only pelican-chameleon theme)
MENUITEMS = (
    ('Instrument', [
        ('SPICE', '/pages/instrument/spice.html'),
        ('Consortium', '/pages/instrument/consortium.html'),
        ('Documents', '/pages/instrument/documents.html'),
        ('Solar Orbiter', '/pages/instrument/solar-orbiter.html'),
    ]),
    ('Science', [
        ('Objectives', '/pages/science/objectives.html'),
        ('Results and publications', '/pages/science/results.html'),
    ]),
    ('Meetings','/pages/meetings.html'),
    ('Operations', [
        ('Instrument status', '/pages/operations/instrument-status.html'),
        ('Planned operations', '/pages/operations/planned-operations.html'),
    ]),
    ('Data', [
        ('Archives', '/pages/data/archives.html'),
        ('Analysis software', '/pages/data/analysis-software.html'),
        ('Analysis user guide', '/pages/data/analysis-user-guide.html'),
    ]),
    ('Outreach','/pages/outreach.html'),
    ('Help', [
        ('Help desk and Contact', '/pages/help/helpdesk.html'),
        ('Wiki', '/pages/help/wiki.html')
    ]),
)


# for pelican-chameleon theme
THEME = 'themes/pelican-chameleon'
CSS_OVERWRITE = '/theme/css/bootstrap.spacelab.min.css'


# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Blogroll
LINKS = (('Solar Orbiter at ESA', 'http://sci.esa.int/solar-orbiter/'),
         ('Solar Orbiter at CNES', 'https://solar-orbiter.cnes.fr'),
         ('Solar Orbiter at NASA', 'https://science.nasa.gov/missions/solar-orbiter'),
        )

# Social widget (to be changed later)
SOCIAL = (('SPICE Twitter', 'https://twitter.com/esascience'),
          ('SPICE Facebook', 'https://www.facebook.com/pages/Solar-Orbiter/622590661130281'),)

DEFAULT_PAGINATION = False

# Uncomment following line if you want document-relative URLs when developing
#RELATIVE_URLS = True
