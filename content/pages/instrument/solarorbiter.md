Title: The Solar Orbiter mission
Summary: Presentation of the Solar Orbiter mission.
Slug: instrument/solar-orbiter

Solar Orbiter is intended to brave the fierce heat and carry its telescopes to nearly one quarter of the Earth's distance from the Sun, where sunlight will be twenty times more intense than satellites in the vicinity of the Earth feel it. The spacecraft must also endure powerful bursts of atomic particles from explosions in the solar atmosphere. The reward will come in the form of sharp images obtained together with unprecedented measurements of the local near-Sun phenomena.
